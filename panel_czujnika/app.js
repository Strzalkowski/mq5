var DEVICE_ID=44;
//adres
const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);
if(urlParams.has('id'))
{
    DEVICE_ID = Number(urlParams.get('id'));
}
const title = document.querySelector("#title");
title.textContent = "Panel Aparatu Być Może Wąchającego o id:" + DEVICE_ID;
// MQTT dependency https://github.com/mqttjs/MQTT.js
//const mqtt = require("mqtt");

// client, user and device details

const serverUrl   = "ws://34.88.143.118:8080";
const clientId    = "browser_client_"+Math.random().toString(36).slice(2);
const device_name = "My Node.js MQTT device";
const tenant      = "<<tenant>>";
const username    = "<<username>>";
const password    = "<<password>>";

const TOPIC_TEMP="mq5/temperature/"+DEVICE_ID;
const TOPIC_PRESS="mq5/pressure/"+DEVICE_ID;
const TOPIC_GAS="mq5/gas/"+DEVICE_ID;
const TOPIC_ALARM_LEVEL="mq5/alarm_level/"+DEVICE_ID;
const TOPIC_ALARM_DURATION="mq5/alarm_duration/"+DEVICE_ID;

var voltage_chart=null;
var temp_press_chart=null;
var temperature   = 25;


// connect the client to Cumulocity
const client = mqtt.connect(serverUrl, {
    username: tenant + "/" + username,
    password: password,
    clientId: clientId
});
// once connected...
client.on("connect", function () {
    console.log("Masaraksz");

    // subscribe...
    client.subscribe(TOPIC_TEMP);
    client.subscribe(TOPIC_PRESS);
    client.subscribe(TOPIC_GAS);
    client.subscribe(TOPIC_ALARM_DURATION);


    client.publish("panel/status", "Panel aktywny.");
});

// display all incoming messages
client.on("message", function (topic, message) {
    var num = Number(message);
    if(num!=0.0)
    {
        var today = new Date(Date.now());
        var datestr = moment(today).format('hh:mm::ss');
        if(topic == TOPIC_TEMP)
        {
            console.log("temperatura:"+message);
            var tab0 = temp_press_chart.data.datasets[0].data;
            var tab1 = temp_press_chart.data.datasets[1].data;
            var labels = temp_press_chart.data.labels;
            //tab.push({x:datestr,y:Number(message)});
            tab0.push(num);
            tab1.push(tab1[tab1.length-1]);
            labels.push(datestr);
            if(tab0.length>40){tab0.shift();tab1.shift();labels.shift();}
            temp_press_chart.update();
        }
        if(topic == TOPIC_PRESS)
        {
            num=num/100;
            console.log("ciśnienie:"+message);
            /*var tab = temp_press_chart.data.datasets[1].data;
            //tab.push({x:datestr,y:Number(message)});
            if(tab.length>40){tab.shift();}
            temp_press_chart.update();*/
            var tab0 = temp_press_chart.data.datasets[0].data;
            var tab1 = temp_press_chart.data.datasets[1].data;
            var labels = temp_press_chart.data.labels;
            //tab.push({x:datestr,y:Number(message)});
            tab0.push(tab0[tab0.length-1]);
            tab1.push(num);
            labels.push(datestr);
            if(tab0.length>40){tab0.shift();tab1.shift();labels.shift();}
            temp_press_chart.update();
        }
        if(topic == TOPIC_GAS)
        {
            console.log("gaz:"+message);
            console.log("#"+datestr);
            var x = voltage_chart.data.labels;
            var y = voltage_chart.data.datasets[0].data;
            x.push(datestr);
            y.push(num);
            if(x.length>40)
            {
                x.shift();
                y.shift();
            }
            voltage_chart.update();
    }
    }
    
    console.log('Received operation "' + message + '"@'+topic);
    
});

const value = document.querySelector("#mV_alarm_display")
const input = document.querySelector("#alarm_level")
value.textContent = input.value
input.addEventListener("input", (event) => {
  value.textContent = event.target.value+"mV";
  console.log("pub"+event.target.value);
  client.publish(TOPIC_ALARM_LEVEL, ""+event.target.value);
})
