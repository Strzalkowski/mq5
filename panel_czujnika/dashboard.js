/* globals Chart:false, feather:false */
var timeFormat = 'moment.ISO_8601';
(function () {
  'use strict'

  feather.replace({ 'aria-hidden': 'true' })

  // Graphs
  var ctx = document.getElementById('myChart')
  // eslint-disable-next-line no-unused-vars
  voltage_chart = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        //Date.now()
      ],
      datasets: [{
        data: [
          //15,
        ],
        lineTension: 0,
        backgroundColor: 'transparent',
        borderColor: '#007bff',
        borderWidth: 4,
        pointBackgroundColor: '#007bff'
      }]
    },
    options: {
      scales: {
        x: {
          type: 'time',
          time: {
          //    unit: 'second'//,
              //parser:''
              //format: timeFormat,
              displayFormats: {
              hour: 'HH:mm:ss'
          }
        },
        ticks: {
          autoSkip: true,
          maxTicksLimit: 20
      }
        
      },
        yAxes: [{
          ticks: {
            beginAtZero: false
          }
        }]
      },
      legend: {
        display: false
      }
    }
  })

  var ctx2 = document.getElementById('myChart2')
  temp_press_chart = new Chart(ctx2, {
    type: 'line',
  data: {
    //labels: ['1', '2', '3', '4', '5'],
    labels:[],
    datasets: [{
      label: 'temp',
      yAxisID: 'temp',
      //data: [{x:'02:02:02',y:12}, {x:'02:02:03',y:15}, {x:'02:02:04',y:12}]
      data:[],
      borderColor: 'rgba(235, 162, 54, 0.9)',
      backgroundColor: 'rgba(235, 162, 54, 0.5)',
    }, {
      label: 'pres',
      yAxisID: 'pres',
      //data: [{x:'02:02:02',y:340}, {x:'02:02:05',y:360}]
      data:[],
      borderColor: 'rgba(54, 162, 235, 0.9)',
      backgroundColor: 'rgba(54, 162, 235, 0.5)',
    }]
  },
  options: {
    scales: {
      yAxes: [{
        id: 'temp',
        type: 'linear',
        position: 'left',
      }, {
        id: 'pres',
        type: 'linear',
        position: 'right'//,
        //ticks: {
        //  max: 1,
        //  min: 0
        //}
      }]
    }
  }
  })


})()
