const FILENAME = "./log.txt";
var fs = require('fs');
var writeStream = fs.createWriteStream(FILENAME,{flags:'a'});
//fs.createWriteStream(__dirname + "/uploading.txt");

var http = require('http'); // Import Node.js core module
const mqtt = require("mqtt");

var server = http.createServer(function (req, res) {   //create web server
    if (req.url == '/') { //check the URL of the current request
        
        // set response header
        res.writeHead(200, { 'Content-Type': 'text/plain' }); 
        var content = fs.readFileSync(FILENAME);
        // set response content    
        res.write(content);
        console.log("resp@"+Date.now());
        res.end();
    
    }
    else if (req.url == "/clear") {
        
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write('<html><body><p>Cleared.</p></body></html>');
        res.end();
        fs.truncate(FILENAME, 0, function(){console.log('cleared');})
    
    }
    else if (req.url == "/admin") {
        
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write('<html><body><p>This is mqtt listener.</p></body></html>');
        res.end();
    
    }
    else
        res.end('Invalid Request!');

});

server.listen(80); //6 - listen for any incoming requests

console.log('Node.js web server at port 80 is running..')

//const mqtt = require("mqtt");

// client, user and device details

const serverUrl   = "ws://34.88.143.118:8080";
const clientId    = "listener"+Math.random().toString(36).slice(2);
const device_name = "Listener";
const tenant      = "<<tenant>>";
const username    = "<<username>>";
const password    = "<<password>>";

const TOPIC_TEMP="mq5/temperature/*";
const TOPIC_PRESS="mq5/pressure/*";
const TOPIC_GAS="mq5/gas/*";
const TOPIC_ALARM_LEVEL="mq5/alarm_level/*";
const TOPIC_ALARM_DURATION="mq5/alarm_duration/*";




// connect the client
const client = mqtt.connect(serverUrl, {
    username: tenant + "/" + username,
    password: password,
    clientId: clientId
});
// once connected...
client.on("connect", function () {
    console.log("Connected");

    // subscribe...
    client.subscribe("mq5/#");
    //client.subscribe(TOPIC_TEMP);
    //client.subscribe(TOPIC_PRESS);
    //client.subscribe(TOPIC_GAS);
    //client.subscribe(TOPIC_ALARM_DURATION);


    client.publish("listener/status", "Podsłuchiwacz aktywny.");
});

client.on("message", function (topic, message) {
    var content = "{["+topic+"],["+message+"],["+Date.now()+"]}\n";
    console.log("->"+content);
    writeStream.write(content);
});